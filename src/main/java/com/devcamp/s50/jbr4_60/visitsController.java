package com.devcamp.s50.jbr4_60;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class visitsController {
    @CrossOrigin
    @GetMapping("/visits")
    public ArrayList<Visit> getVisitApi(){
        Customer customer1 = new Customer("Duong");
        Customer customer2 = new Customer("xuan");
        Customer customer3 = new Customer("dao");

        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3);

        Visit visit1 = new Visit(customer1, new Date());
        Visit visit2 = new Visit(customer2,  new Date() , 3, 12);
        Visit visit3 = new Visit(customer3,  new Date() , 6, 52);

        System.out.println(visit1);
        System.out.println(visit2);
        System.out.println(visit3);

        ArrayList<Visit> listsVisit = new ArrayList<>();
        listsVisit.add(visit1);
        listsVisit.add(visit2);
        listsVisit.add(visit3);

        return listsVisit ;
    }
}
