package com.devcamp.s50.jbr4_60;

public class Customer {
    private String name;
    private boolean member = false ;
    private String menberType ;
    
    public Customer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isMember() {
        return member;
    }

    public String getMenberType() {
        return menberType;
    }

    public void setMember(boolean member) {
        this.member = member;
    }

    public void setMenberType(String menberType) {
        this.menberType = menberType;
    }

    @Override
    public String toString() {
        return "Customer [name=" + name + ", member=" + member + ", menberType=" + menberType + " ]";
    }
    
}
